# Frame stack

This is another simple program which overlays every frame of video using designated blend modes and opacities to create an interesting effect that seems to blur time.