#!/usr/local/bin/python
# -*-encoding:UTF-8-*-

# Evan Krause 8/20/2014

from PIL import Image
from PIL import ImageChops
import glob

import sys
import os

def usage():
    # Make sure the glob is in quotes
    print 'Usage: python framestack.py "path/to/*.png" path/to/output/ [0.8]'
    print "Make sure the glob is in quotes"

def write(s):
    sys.stdout.write("\033[0G")
    sys.stdout.flush()
    sys.stdout.write(' ' + s)

def pbar(count, l):
    length = 60
    p = int((float(count) / l) * length)
    done = '#' * p
    notdone = ' ' * (length - p)
    s = "[\033[32m%s\033[0m%s] %04d/%04d" % (done, notdone, count, l)
    write(s)

SUBSCREEN = 0
SCREEN = 1
ADD = 2
MULTIPLY = 3
SUBTRACT = 4
SCREENSUB = 5
ALPHA = 6

MODES = [
        SUBSCREEN,
        SCREEN,
        ADD,
        MULTIPLY,
        SCREENSUB,
        ALPHA,
        ]

def run(globpath, outpath, mult=None, mode=SUBSCREEN):
    plate = None
    count = 0

    if not os.path.exists(outpath):
        print " > making dir %s" % outpath
        os.makedirs(outpath)

    g = glob.glob(globpath)
    l = len(g)
    if l == 0:
        print "No images found from %s" % globpath
        sys.exit()
    for f in g:
        im = Image.open(f)
        im = im.convert('RGBA')
        if not plate:
            plate = Image.new(im.mode, im.size, (0,0,0,0))
        if mult:
            def darken(p):
                return float(mult) * p
            plate = Image.eval(plate, darken)
        if mode==SUBSCREEN:
            plate = ImageChops.subtract(plate, im)
            plate = ImageChops.screen(plate, im)
        elif mode==SCREEN:
            plate = ImageChops.screen(plate, im)
        elif mode==ADD:
            plate = ImageChops.add(plate, im)
        elif mode==MULTIPLY:
            plate = ImageChops.multiply(plate, im)
        elif mode==SUBTRACT:
            plate = ImageChops.subtract(plate, im)
        elif mode==SCREENSUB:
            plate = ImageChops.screen(plate, im)
            plate = ImageChops.subtract(plate, im)
        else:
            #plate = Image.blend(plate, im, 1.0)
            #plate = plate.paste(im, (0,0), im)
            plate = Image.alpha_composite(plate, im)

        count += 1
        pbar(count, l)
        if l >= 10:
            plate.save(outpath + '%02d.png' % count)
        elif l >= 100:
            plate.save(outpath + '%03d.png' % count)
        elif l >= 1000:
            plate.save(outpath + '%04d.png' % count)
        else:
            plate.save(outpath + '%01d.png' % count)
    print

# dir that holds folders of frames,
# dir to output to
def batch(g, outpath):
    import time
    start = time.time()
    print "batch process %s --> %s" % (str(g), str(outpath))
    arr = [x[0] for x in os.walk(g)][1:]
    todo = len(arr)
    count = 0
    for f in arr:
        count += 1
        inp = f + '/*.png'
        bf = os.path.basename(f)
        out = "%s/%s/" % (outpath, bf)
        print "Running %s  (%02d/%02d)" % (bf, count, todo)
        run(inp, out, mode=ALPHA)
    def formattime(f):
        #print f
        import math
        hrs = math.floor(f / (60*60))
        mins = math.floor(f / 60)
        secs = math.floor(f % 60)
        secsd = "%02f" % (f % 60)
        return "%02d:%02d:%02d.%s" % (hrs, mins, secs, secsd.split('.')[1][:2])
    print "\033[33mDone in %s\033[0m" % (formattime(time.time()-start))

if __name__ == '__main__':
    if len(sys.argv) == 4:
        if sys.argv[1] == 'batch':
            batch(sys.argv[2], sys.argv[3])
        sys.exit()
    if len(sys.argv) < 3:
        usage()
        sys.exit()

    globpath = sys.argv[1]
    outpath = sys.argv[2]
    if outpath[-1:] != '/':
        outpath += '/'
    if len(sys.argv) > 3:
        try:
            mult = float(sys.argv[3])
        except:
            usage()
            print "Multiplier must be a number! not %s, dummy" % str(sys.argv[3])
            sys.exit()

    mult = None
    if (len(sys.argv) >= 4):
        mult = sys.argv[3]
    mode = SUBSCREEN
    if (len(sys.argv) >= 5):
        mode = sys.argv[4]
    print "g:%s o:%s m:%s m:%s" % (globpath, outpath, mult, mode)
    run(globpath, outpath, mult, mode)
